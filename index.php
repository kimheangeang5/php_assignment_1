<?php
session_start();
    $servername='localhost';
    $username='root';
    $password='';
    $dbname = "note";
    $conn=mysqli_connect($servername,$username,$password,"$dbname");
    if(!$conn){
       die('Could not Connect My Sql:' .mysql_error());
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bootstrap Simple Login Form</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
</style>
</head>
<body>
<?php
    $error = '';
    if(isset($_POST['save'])) { 
        $username = $_POST['username'];
        $password = $_POST['password'];
        $userId = "";
        $result = mysqli_query($conn,"select * from tbl_user where username='$username' and password='$password'") or die(mysqli_error());
        while($row = mysqli_fetch_array($result)){
            if($row['username'] == $username) {

                if($row['password'] == $password){
                    //header("location: home.php?id=");
                     $_SESSION['userId']=$username;
                     $_SESSION['id']=$row['id'];
                    header("Location: home.php?id=".$row['id']."");
                    exit;
                }
            }
        }
        if(mysqli_num_rows($result)==0){
            $error="Incorrect username and password...";
        }
    }
?>
<div class="login-form">
    <form action="/assignment/note/index.php" method="post">
        <h2 class="text-center">Log in</h2>  
        <p style="color: red; font-size:14px;"><?php echo $error?></p>     
        <div class="form-group">
            <input name="username" type="text" class="form-control" placeholder="Username" required="required">
        </div>
        <div class="form-group">
            <input name="password" type="password" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block" name="save">Log in</button>
        </div>
    </form>
    <p class="text-center"><a href="/assignment/note/signup.php">Create an Account</a></p>
</div>
</body>
</html>                                		                            
