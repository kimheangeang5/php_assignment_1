<?php session_start();?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Keep Notes</title>
    <style>
        #more{
            padding-right: 0px;
        }

textarea {
  width: 97%;
  padding: 10px;
  font-size: 20px;
  border: none;
  font-family: "Times New Roman", Times, serif;
  resize: none;
  overflow: hidden;
}

input[type=text] {
  width: 100%;
  font-weight: bold;
  box-sizing: border-box;
  font-size: 24px;
  background-color: white;
  font-family: "Times New Roman", Times, serif;
  border: none;
  background-position: 10px 10px;
  background-repeat: no-repeat;
  padding: 10px;
}

textarea:focus, input:focus{
    outline: none;
}
.image-upload > input
{
    display: none;
}

.image-upload img
{
    width: 80px;
    cursor: pointer;
}
.boder {
    margin: auto;
    margin-top: 30px;
   padding: 10px;
   width: 700px;
   box-shadow: 7px 7px 20px #888888;
   border-radius: 10px;
   border: 2px solid #ffffff;
}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 16px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;
}
.button5:hover {
  background-color: #555555;
  color: white;
}

.input-container {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  width: 100%;
  margin-bottom: 15px;
}

.icon {
  padding: 10px;
  color: #999999;
  min-width: 50px;
  text-align: center;
}
.icon:hover {
  color: #4d4d4d;
}

    </style>
</head>
<script
    src="https://code.jquery.com/jquery-1.8.3.js"
    integrity="sha256-dW19+sSjW7V1Q/Z3KD1saC6NcE5TUIhLJzJbrdKzxKc="
    crossorigin="anonymous"></script>
<body>
    <?php

    $owner =  $_SESSION['id'];

            $servername='localhost';
            $username='root';
            $password='';
            $dbname = "note";
            $conn=mysqli_connect($servername,$username,$password,"$dbname");
            if(!$conn){
               die('Could not Connect My Sql:' .mysql_error());
            }

            $result = mysqli_query($conn,"SELECT * FROM tbl_note WHERE owner = $owner");

    
           
            function updateNote($title,$content,$image,$noteId){
                    mysqli_query($conn,"update tbl_note set title=$title,content=$content,image=$image where id=$noteId") or die(mysqli_error());
            }
             ?>
    <nav class="navbar navbar-light justify-content-between" style="background-color: #e3f2fd;">
        <!-- <img src="/assignment/note/images/background.jpg" alt="user" class="rounded-circle navbar-brand" width="30" height="30"> -->
        <a class="navbar-brand">Keep your note</a>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            <img src="/assignment/note/images/background.jpg" alt="user" class="rounded-circle ml-2" width="30"
                height="30">
        </form>
    </nav>
    <!-- You can get the user id by this code -->
    

    <?php
       $smg = "";

      if(isset($_POST['save'])) { 

         $title = $_POST['title'];
         $content = $_POST['content'];

         $target = "images/".basename($_FILES['image']['name']);
         $image = $_FILES['image']['name'];

          mysqli_query($conn,"insert into tbl_note (title,content,image,owner) 
                   values ('$title','$content','$image',$owner)") or die(mysqli_error());
          if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
                   $smg = "Image Upload Successfully!";
          } else {
                    $smg = "There was a Failed!!!";
          }
          header("Location: home.php?id=".$owner."");
      }
      $noteId = '';
      if(isset($_POST['delete'])) { 
                    $tt = $noteId."hello";
                    print_r($tt);
                    $id = $_POST[$tt];
                    mysqli_query($conn,"DELETE FROM tbl_note WHERE id = $id") or die(mysqli_error());
                    header("Location: home.php?id=".$owner."");
         }  

    ?>

    <div class="boder" >
        <form method="post"  enctype="multipart/form-data">
         
         <div class="input-container">
            
            <input class="input-field" type="text" id="showmenu"  placeholder="Title..." name="title">
            <a href=""><i class="fa fa-plus  icon" aria-hidden="true"></i></a>

            <div class="image-upload">
                 <label for="file-input">
                         <i class="fa fa-picture-o  icon" aria-hidden="true"></i>
                 </label>
                 <input id="file-input" type="file" name="image" />
            </div>
            <a href="">  <i class="fa fa-paint-brush  icon" aria-hidden="true"></i></a>
               
         </div>

            <div class="menu" style="display: none;">
              <textarea placeholder="Yourself Contect here..." onkeyup="auto_grow(this)" name="content"></textarea>
              <button class="button button5" id="btn" type="submit"  name="save"
              >Submit</button>
            </div>
        </form> 
    </div>


     <form method="post"  enctype="multipart/form-data">
    <div class="row" style="padding: 20px; margin-left: 100px">

            <?php
            
                $i=0;
                while($row = mysqli_fetch_array($result)) {
            ?>
            <?php 
                $photo = 'images/'.$row["image"];
                $noteId = $row["id"];

            ?>
                <div class="col-sm-3" style="margin-top: 20px">
                    <div class="card" style="width: 20rem;">
                       <img src="<?php echo $photo; ?>" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title"><?php echo $row["title"]; ?></h5>
                           <p class="card-text"><?php echo $row["content"]; ?></p>

                           <div style="float: right;">
                            
                            <input type='text' name='<?php echo $noteId;?>' value='<?php echo $noteId; ?>'>
                            <button class="button button5" id="btn" type="submit"  name="delete"
                            > <i class="fa fa-trash" aria-hidden="true"></i></button>
                           </div>
                          
                            
                        </div>
                    </div>
                </div>

            <?php
                    $i++;
                }
            ?>
    </div>

    </form>
 
</body>
<script type="text/javascript">



    $(document).ready(function() {

        $('#btn').click(function() {
                $('.menu').hide('slow');
        });

        $('#showmenu').click(function() {
                $('.menu').show(600);
        });
        
    });

    function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}
</script>
</html>
